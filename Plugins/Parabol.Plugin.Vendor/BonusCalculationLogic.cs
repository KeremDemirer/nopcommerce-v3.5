﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Parabol.Plugin.Vendor
{
    public static class BonusCalculationLogic
    {
        public static decimal calculateBonus(decimal price)
        {
            return price * (decimal)0.1;
        }
    }
}
