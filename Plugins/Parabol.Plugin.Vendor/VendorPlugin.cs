﻿using Nop.Core.Data;
using Nop.Core.Plugins;
using Nop.Services.Tasks;
using Nop.Web.Framework;
using Nop.Web.Framework.Menu;
using Parabol.Plugin.Vendor.Data;
using Parabol.Plugin.Vendor.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Parabol.Plugin.Vendor
{
    public class VendorPlugin : BasePlugin
    {
        private AccountingRecordObjectContext _context;
        private IRepository<AccountingRecord> _vendorRep;
        private ScheduleTaskService _scheduleTaskService;

        public VendorPlugin (AccountingRecordObjectContext context, IRepository<AccountingRecord> vendorRepo,ScheduleTaskService sheduleTaskService)
        {
            _context = context;
            _vendorRep = vendorRepo;
            _scheduleTaskService = sheduleTaskService;  
        }
        public bool Authenticate()
        {
            
            return true;
        }

        public override void Install()
        {
            _scheduleTaskService.InsertTask(new Nop.Core.Domain.Tasks.ScheduleTask()
            {
                Enabled = true,
                Name = "Accounting Task",
                Seconds = 3600,
                StopOnError = false,
                Type = "Parabol.Plugin.Vendor.AccountingTask, Parabol.Plugin.Vendor",
            });

            _context.Install();
            base.Install();
        }

        public override void Uninstall()
        {
            Nop.Core.Domain.Tasks.ScheduleTask task = _scheduleTaskService.GetTaskByType("Parabol.Plugin.Vendor.AccountingTask, Parabol.Plugin.Vendor");
            if (task != null)
            {
                _scheduleTaskService.DeleteTask(task);
            }
            _context.Uninstall();
            base.Uninstall();
        }

    }
}
