﻿using Nop.Admin.Models.Orders;
using Nop.Services.Logging;
using Nop.Services.Orders;
using Nop.Services.Tasks;
using Parabol.Plugin.Vendor.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Parabol.Plugin.Vendor
{
    public enum OrderStatus
    {
        /// <summary>
        /// Pending
        /// </summary>
        Pending = 10,
        /// <summary>
        /// Processing
        /// </summary>
        Processing = 20,
        /// <summary>
        /// Complete
        /// </summary>
        Complete = 30,
        /// <summary>
        /// Cancelled
        /// </summary>
        Cancelled = 40
    }

    public class AccountingTask : ITask
    {
        private readonly ILogger _logger;
        private readonly IOrderService _orderService;
        private AccountingModel _accountingModel;
        private Dictionary<int, string> dictionary;

        public AccountingTask(ILogger logger, IOrderService orderService,AccountingModel accountingModel)
        {
            _orderService = orderService;
            _logger = logger;
            _accountingModel = accountingModel;
            dictionary = new Dictionary<int,string>();
            dictionary.Add(10, "Pending");
            dictionary.Add(20, "Processing");
            dictionary.Add(30, "Complete");
            dictionary.Add(40, "Cancelled");
        }
        public void Execute()
        {
            var model = _orderService.GetAllOrderItems(null,null,null,null,null,null,null,false);

            foreach (var item in model)
            {
                if (item.Product.VendorId != 0)
                {
                    //either insert or update if cancelled
                    if(_accountingModel.getByOrderItemId(item.Id) == 0) //not exists => insert
                    {
                        _accountingModel.insert(item.Product.VendorId,
                            item.Id,
                            dictionary[item.Order.OrderStatusId],
                            BonusCalculationLogic.calculateBonus(item.Product.Price));
                    }
                    else if( _accountingModel.getNote(item.Id) != dictionary[item.Order.OrderStatusId] ) //exists and changed
                    {
                        var rowId = _accountingModel.getByOrderItemId(item.Id);
                        _accountingModel.update(rowId,
                            item.Product.VendorId,
                            item.Id,
                            dictionary[item.Order.OrderStatusId],
                            0);
                    }
                }
            }
            

            _logger.InsertLog(Nop.Core.Domain.Logging.LogLevel.Information, "AccountingTask Finished", "AccountingTask ......", null);
        }
        
          
    }
}
