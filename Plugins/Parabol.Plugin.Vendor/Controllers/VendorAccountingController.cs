﻿using Nop.Core.Data;
using Nop.Web.Framework.Controllers;
using Nop.Web.Framework.Kendoui;
using Nop.Web.Framework.Mvc;
using Parabol.Plugin.Vendor.Domain;
using Parabol.Plugin.Vendor.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace Parabol.Plugin.Vendor.Comtrollers
{
   public class VendorAccountingController : BasePluginController
    {
        private IRepository<AccountingRecord> _vendorsRepository;
        private AccountingModel _accountingModel;
        public VendorAccountingController(IRepository<AccountingRecord> vendorsRepository)
        {
            _vendorsRepository = vendorsRepository;
            _accountingModel = new AccountingModel(vendorsRepository);
        }

        [HttpPost]
        public ActionResult List(int vendorId, DataSourceRequest command)
        {
            var accountingRecords = new List<AccountingRecord>();
            var accountings = _accountingModel.getRecordsByVendorId(vendorId);
            
            var gridModel = new DataSourceResult
            {
                Data = accountings,
                Total = accountings.Count()
            };

            return Json(gridModel);
        }

        [HttpPost]
        public ActionResult Add(int vendorId, decimal credit, decimal debit,string note)
        {
            _accountingModel.insert(vendorId, null, note, credit, debit);
            return Json(new { Result = true }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult Update([Bind(Exclude = "ConfigurationRouteValues")] AccountingRecord model)
        {
            _accountingModel.update(model.Id, model.VendorId, null, model.Notes, model.Credit, model.Debit);
            return Json(new { Result = true }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult Delete(int id)
        {
            _accountingModel.delete(id);
            return new NullJsonResult();
        }




    }
}
