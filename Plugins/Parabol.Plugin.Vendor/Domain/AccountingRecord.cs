﻿using Nop.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Parabol.Plugin.Vendor.Domain
{
    public class AccountingRecord : BaseEntity
    {
        public int VendorId { get; set; }
        public int? OrderItemId { get; set; }
        public decimal Debit { get; set; }
        public decimal Credit { get; set; }
        public DateTime CreatedOnUtc { get; set; }
        public DateTime UpdatedOnUtc { get; set; }
        public string Notes { get; set; }

    }
}
