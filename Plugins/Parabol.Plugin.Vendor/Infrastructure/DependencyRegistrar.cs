﻿using Autofac;
using Nop.Core.Infrastructure;
using Nop.Core.Infrastructure.DependencyManagement;
using Parabol.Plugin.Vendor.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Nop.Web.Framework.Mvc;
using Parabol.Plugin.Vendor.Domain;
using Nop.Data;
using Nop.Core.Data;
using Autofac.Core;
using Nop.Services.Tasks;
using Parabol.Plugin.Vendor.Models;

namespace Parabol.Plugin.Vendor.Infrastructure
{
    public class DependencyRegistrar : IDependencyRegistrar
    {
        private const string CONTEXT_NAME = "nop_object_context_product_view_tracker";

        public virtual void Register(ContainerBuilder builder, ITypeFinder typeFinder)
        {
           //  builder.RegisterType<AccountingTask>().As<AccountingTask>().InstancePerLifetimeScope();
            builder.RegisterType<ScheduleTaskService>().As<ScheduleTaskService>(); 
            builder.RegisterType<AccountingModel>().As<AccountingModel>();
            //data context
            this.RegisterPluginDataContext<AccountingRecordObjectContext>(builder, CONTEXT_NAME);

            //override required repository with our custom context
            builder.RegisterType<EfRepository<AccountingRecord>>()
                .As<IRepository<AccountingRecord>>()
                .WithParameter(ResolvedParameter.ForNamed<IDbContext>(CONTEXT_NAME))
                .InstancePerLifetimeScope();
        }

        public int Order
        {
            get { return 1; }
        }
    }
}
