using Nop.Web.Framework.Themes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Parabol.Plugin.Vendor.Infrastructure
{
    public class CustomViewEngine : ThemeableRazorViewEngine
    {
        public CustomViewEngine()
        {
            ViewLocationFormats = new[] { "~/Plugins/Parabol.Vendor/Views/{0}.cshtml" };
            PartialViewLocationFormats = new[] { "~/Plugins/Parabol.Vendor/Views/{0}.cshtml" };
        }
    }
}
