﻿using Nop.Web.Framework.Mvc.Routes;
using Parabol.Plugin.Vendor.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using System.Web.Routing;

namespace Parabol.Plugin.Vendor
{
    public class RouteConfig : IRouteProvider
    {
        public int Priority
        {
            get
            {
                return 0;
            }
        }

        public void RegisterRoutes(RouteCollection routes)
        {
            routes.MapRoute("Parabol.Plugin.Vendor",
                 "Plugins/VendorAccounting/List",
                 new { controller = "VendorAccounting", action = "List" },
                new[] { "Parabol.Plugin.Vendor.Controllers" });
            ViewEngines.Engines.Insert(0, new CustomViewEngine());
        }
    }
}
