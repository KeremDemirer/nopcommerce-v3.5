﻿using Parabol.Plugin.Vendor.Domain;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Parabol.Plugin.Vendor.Data
{
    public class AccountingRecordMap : EntityTypeConfiguration<AccountingRecord>
    {
        public AccountingRecordMap()
        {
            ToTable("PrblVendorAccounting");
            HasKey(m => m.Id);
            Property(m => m.VendorId);
            Property(m => m.OrderItemId);
            Property(m => m.Debit);
            Property(m => m.Credit);
            Property(m => m.CreatedOnUtc);
            Property(m => m.UpdatedOnUtc);
            Property(m => m.Notes);
        }
    }
}
