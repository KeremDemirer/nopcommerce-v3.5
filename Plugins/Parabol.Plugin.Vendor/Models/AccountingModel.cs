﻿using Nop.Core.Data;
using Parabol.Plugin.Vendor.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Parabol.Plugin.Vendor.Models
{
    public class AccountingModel
    {
        private IRepository<AccountingRecord> _vendorsRepository;
        public AccountingModel(IRepository<AccountingRecord> vendorsRepository)
        {
            _vendorsRepository = vendorsRepository;
        }

        public List<AccountingRecord> getRecordsByVendorId(int vendorId)
        {
           var results = _vendorsRepository.Table.Where(x => x.VendorId == vendorId).OrderByDescending(ac => ac.CreatedOnUtc).ToList();
           return results;
        }

        public void insert(int vendorId,int? orderItemId,string notes, decimal credit = 0, decimal debit = 0)
        {
            _vendorsRepository.Insert(new AccountingRecord()
            {
                VendorId = vendorId,
                OrderItemId = orderItemId,
                Notes = notes,
                CreatedOnUtc = DateTime.Now,
                UpdatedOnUtc = DateTime.Now,
                Credit = credit,
                Debit = debit
            });
            
        }

        public void update(int id,int vendorId, int? orderItemId, string notes, decimal credit = 0, decimal debit = 0)
        {
            var record = _vendorsRepository.GetById(id);
            record.VendorId = vendorId;
            record.OrderItemId = orderItemId;
            record.Notes = notes;
            record.UpdatedOnUtc = DateTime.Now;
            record.Credit = credit;
            record.Debit = debit;
            _vendorsRepository.Update(record);
        }

        public int getByOrderItemId(int orderItemId)
        {
            var record = _vendorsRepository.Table.First(x => x.OrderItemId == orderItemId);
            if (record != null)
                return record.Id;
            return 0;
        }

        public string getNote(int orderItemId)
        {
            var record = _vendorsRepository.Table.First(x => x.OrderItemId == orderItemId);
            if (record != null)
                return record.Notes;
            return "";
        }

        public void delete(int id)
        {
            var record = _vendorsRepository.Table.First(x => x.Id == id);
            if (record != null)
                _vendorsRepository.Delete(record);
        }
    }
}
