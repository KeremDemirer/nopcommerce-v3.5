
namespace Nop.Core.Domain.Orders
{
    /// <summary>
    /// Represents a shoping cart type
    /// </summary>
    public enum ShoppingCartType
    {
        /// <summary>
        /// Shopping cart
        /// </summary>
        ShoppingCart = 1,
        /// <summary>
        /// Wishlist
        /// </summary>
        Wishlist = 2,
        /// <summary>
        /// Customized
        /// </summary>
        Customized = 3,
        /// <summary>
        /// Collection
        /// </summary>
        Collection = 4
    }
}
